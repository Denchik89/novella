package Programma;
import Programma.Vstrecha.*;
import Programma.Vstrecha.Drug.*;
import Programma.Vstrecha.Nicto.*;
import Programma.Vstrecha.Vrag.*;

public class Dvizhok {

  private Node start;
  private Vstrecha vstrecha;


  public Node getDerevo() throws Exception {
    return this.start;
  }

  public void setDerevo(Node start) {
    this.start = start;
  }

  public void dvizhok() throws Exception {
    vstrecha = new Vstrecha("Встреча");
    this.start = new Node();
    Node level1 = new Node();
    Node level2 = new Node();
    Node level3 = new Node();
    Node level4 = new Node();
    Node level5 = new Node();
    Node level6 = new Node();
    Node level7 = new Node();
    Node level8 = new Node();
    Node level9 = new Node();
    Node level10 = new Node();

    start.addChildren(level1);
    start.addAction(vstrecha.generate());

    level1.addChildren(level2);
    level1.addAction(vstrecha.generate());


    level2.addChildren(level3);
    level2.addAction(vstrecha.generate());

    level3.addChildren(level4);
    level3.addAction(vstrecha.generate());

    level4.addChildren(level5);
    level4.addAction(vstrecha.generate());

    level5.addChildren(level6);
    level5.addAction(vstrecha.generate());

    level6.addChildren(level7);
    level6.addAction(vstrecha.generate());

    level7.addChildren(level8);
    level7.addAction(vstrecha.generate());

    level8.addChildren(level9);
    level8.addAction(vstrecha.generate());

    level9.addChildren(level10);
    level9.addAction(vstrecha.generate());
  }

}
