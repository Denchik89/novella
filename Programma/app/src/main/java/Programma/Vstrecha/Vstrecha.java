package Programma.Vstrecha;

import Programma.Vstrecha.Drug.*;
import Programma.Vstrecha.Nicto.*;
import Programma.Vstrecha.Vrag.*;


import java.util.*;
import java.io.*;

public class Vstrecha {
  public String message;
  public ArrayList<Action> actions = new ArrayList<Action>();
  public Action action;

  public Vstrecha(String message){
    this.message = message;
  }

  public double act(double hp)  throws Exception {
    return hp;
  }

  public String getMessage(){
    return this.message;
  }

  public static Vstrecha generate() throws Exception {
    Vstrecha vrag = new Vrag("Попался враг");
    Vstrecha drug = new Drug("Попался друг");
    Vstrecha nicto = new Nicto("Вам никто не попался");

    int randomaizer = (int) (Math.random() * 3);

    if (randomaizer == 0) {
      return drug;
    } else if (randomaizer == 1) {
      return nicto;
    }
    return vrag;
  }

  //добавить метод фор чтоб выводил все актионсы
  public double forAction(double hp) throws Exception {
    int num = 1;
    for (int i = 0; i < this.actions.size(); i++) {
      System.out.println(actions.get(i));
      num++;
    }
    return hp;
  }

  public double procesActions(double hp) throws Exception {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    boolean flag = true;
    int userInput = 0;
    while(flag) {
          try {
          userInput = Integer.parseInt(scanner.readLine());
            if (userInput >= 0 && userInput < actions.size()) {
              action = actions.get(userInput);
              hp = action.act(hp);
              flag = action.isEndless();
            } else {
              System.out.println("Вы ввели неправильно");
              flag = true;
            }
          } catch (NumberFormatException | IOException e) {
              System.out.println("Введите число!");
            }
        }
        System.out.println();
        return hp;
      }
  }
