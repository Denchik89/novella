package Programma.Vstrecha.Nicto;
import Programma.Vstrecha.*;
import java.util.*;
import java.io.*;
import java.util.Scanner;

public class Nicto extends Vstrecha {

  public double act(double hp) throws Exception {
    return hp;
  }

  public Nicto(String message){
    super(message);
    actions.add(new ActionStay("0) постоять"));
    actions.add(new ActionSead("1) посидеть"));
    actions.add(new ActionPerehod("2) перейти на следующий уровень"));
  }
}
