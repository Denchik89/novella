package Programma.Vstrecha.Nicto;
import Programma.Vstrecha.*;

public class ActionStay extends Action {


  ActionStay(String deystvie) {
    super(deystvie);
    endless = true;
  }

  public double act(double hp) throws Exception {
    System.out.println("\n");
    System.out.println("Вы стоите");
    System.out.println("\n");
    return hp;
  }
}
