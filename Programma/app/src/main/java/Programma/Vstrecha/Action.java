package Programma.Vstrecha;
import Programma.Vstrecha.Drug.*;
import Programma.Vstrecha.Nicto.*;
import Programma.Vstrecha.Vrag.*;
import java.util.*;

abstract public class Action {

  public boolean endless;
  protected String deystvie;

  public Action(String deystvie) {
    this.deystvie = deystvie;
  }


  public boolean isEndless() {
    return endless;
  }

  public String toString() {
    return deystvie;
  }

  abstract public double act(double hp) throws Exception;

}
