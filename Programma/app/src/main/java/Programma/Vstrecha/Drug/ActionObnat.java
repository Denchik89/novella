package Programma.Vstrecha.Drug;
import Programma.Vstrecha.*;

public class ActionObnat extends Action {


  ActionObnat(String deystvie) {
    super(deystvie);
  }

  public double act(double hp) throws Exception {
    if (hp <= 50) {
      hp += 50;
      System.out.println("\n");
      System.out.println("Ваше хп пополнилось на 50 процентов" + "\n" +
                         "Теперь хп равняется = " + hp);
      System.out.println("\n");
    } else {
      hp += 0;
      System.out.println("Прости, я не могу тебе пополнить hp, так как у тебя больше 50 процентов");
    }

    return hp;
  }

}
