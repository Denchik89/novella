package Programma.Vstrecha.Drug;
import Programma.Vstrecha.*;
import Programma.*;
import java.util.*;
import java.io.*;
import java.util.Scanner;

public class Drug extends Vstrecha {

  public double act(double hp) throws Exception {
    return hp;
  }

  public Drug(String message){
    super(message);
    actions.add(new ActionHello("0) поздороваться"));
    actions.add(new ActionObnat("1) обнять(пополните хп на 50 процентов)"));
  }
}
