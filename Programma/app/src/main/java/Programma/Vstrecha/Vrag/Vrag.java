package Programma.Vstrecha.Vrag;
import Programma.Vstrecha.*;
import java.util.*;
import java.io.*;
import java.util.Scanner;

public class Vrag extends Vstrecha {


  public Vrag(String message){
    super(message);
    actions.add(new ActionPersonAttack("0) Нанести удар по врагу(урон равен 25)"));
    actions.add(new ActionPersonZashita("1) Защититься от удара врага(снижает урон от врага на 10)"));
  }


  public double procesActions(double hp) throws Exception {
    double personAttack = 25;

    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    boolean flag = true;
    int userInput = 0;
    double hpRobot = 100;
    while(flag) {
      try {
        userInput = Integer.parseInt(scanner.readLine());
        if (userInput >= 0 && userInput < 1) {
          action = actions.get(userInput);
          hp = action.act(hp);
          System.out.println("-------------------------------------------------------" + "\n");
          System.out.print("Вы нанесли врагу " + personAttack + " урона" + "\n" +
                           "Враг нанес вам 15" + "\n" +
                           "У вас осталось " + hp + "hp" + "\n");
          hpRobot -= personAttack;
        } else if (userInput >= 1 && userInput < 2) {
          action = actions.get(userInput);
          hp = action.act(hp);
          System.out.print("Вы защитились" + "\n" +
                             "Враг нанес вам " + " 5 " + "\n" +
                             "у вас осталось " + hp + "hp" + "\n");
        } else {
          System.out.println("Введите 0 либо 1");
        }
        System.out.println("У врага осталось " + hpRobot + "hp" + "\n");
        System.out.println("-------------------------------------------------------" + "\n");

        if (hp <= 0) {
          System.out.println("Вы проиграли");
          flag = false;
        } else if (hpRobot <= 0) {
          System.out.println("Вы выиграли");
          flag = false;
        }
      } catch (NumberFormatException | IOException e) {
          System.out.println("Введите число!");
        }
    }
    return hp;
  }

}







































  // public double act(double hp) throws Exception {
  //   String name = "ваши действия: " + "\n" +
  //                 "1) действие: атака(урон в диапозоне 10-30)" + "\n" +
  //                 "2) действие: защита(снижает полученый урон от робота в диапозоне 5-10)";
  //   System.out.println(name);
  //
  //
  //   personHp = hp;
  //
  //
  //   boolean flagGame = true;
  //   while (flagGame) {
  //     double personAttack = 15;
  //     double robotAttack = 10;
  //     double personZashita = 20;
  //     personAttack += Math.random() * 10;
  //     robotAttack += Math.random() * 5;
  //     personZashita += Math.random() * 30;
  //     int personInput = 0;
  //       personInput = checkingInt("Введите номер действия:", personInput);
  //       if (personInput == 1) {
  //         System.out.println("-------------------------------------------------------" + "\n");
  //         this.robotHp = personAttack(robotHp, personAttack);
  //         System.out.print("Вы нанесли врагу " + personAttack + " урона" + "\n" +
  //                            "У врага осталось " + robotHp + "hp" + "\n");
  //         System.out.println("-------------------------------------------------------" + "\n");
  //         this.personHp = robotAttack(personHp, robotAttack, robotHp);
  //         System.out.println("Враг нанес " + robotAttack + " урона" + "\n" +
  //                            "Осталось у вас " + personHp + "hp");
  //
  //         System.out.println("-------------------------------------------------------" + "\n");
  //
  //       } else if (personInput == 2) { //при защите проблема с кодом
  //           System.out.println("-------------------------------------------------------" + "\n");
  //           this.personHp = personZashita(personHp, robotAttack, personZashita);
  //           System.out.println("Вы защитились" + "\n" +
  //                              "Враг нанес вам " + (robotAttack - personZashita) + "\n" +
  //                              "у вас осталось " + personHp + "hp");
  //
  //           System.out.println("-------------------------------------------------------" + "\n");
  //         }
  //       if (personHp <= 0) {
  //         System.out.println("Вас убили");
  //         System.out.println("\n");
  //         flagGame = false;
  //         robotHp = 100;
  //       } else if (robotHp <= 0) {
  //         System.out.println("Вы убили врага");
  //         System.out.println("\n");
  //         flagGame = false;
  //         robotHp = 100;
  //       }
  //   }
  //   return personHp;
  //
  // }
