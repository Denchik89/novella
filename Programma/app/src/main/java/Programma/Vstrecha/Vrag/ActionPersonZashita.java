package Programma.Vstrecha.Vrag;
import Programma.Vstrecha.*;

public class ActionPersonZashita extends Action {


  ActionPersonZashita(String deystvie) {
    super(deystvie);
    endless = true;
  }

  public double act(double hp) throws Exception {
    double robotAttack = 15;
    double personZashita = 10;
    return hp - (robotAttack - personZashita);
  }
}
