package Programma.Vstrecha.Vrag;
import Programma.Vstrecha.*;

public class ActionPersonAttack extends Action {


  ActionPersonAttack(String deystvie) {
    super(deystvie);
    endless = true;
  }

  public double act(double hp) throws Exception {
    double robotAttack = 15;
  
    return hp - robotAttack;
  }
}
