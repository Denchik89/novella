package Programma;
import Programma.Vstrecha.*;
import Programma.Vstrecha.Drug.*;
import Programma.Vstrecha.Nicto.*;
import Programma.Vstrecha.Vrag.*;

import java.util.Scanner;
import java.util.*;
import java.io.*;

public class Main {



  public static void main(String[] args) throws Exception {
    BufferedReader scanner = new BufferedReader(new InputStreamReader(System.in));
    Dvizhok dvizhok = new Dvizhok();
    dvizhok.dvizhok();
    Hero hero;
    int num = 0;
    int point = 1;
    double heroHp = 0;

    System.out.println("Приветствую тебя игрок " + "\n" +
                       "Думаю ты знаешь уже условия игры, так что введи свое имя и можем начинать");
    while(true) {
      try {
        String userInput = scanner.readLine();
        if (userInput != null && userInput.length() != 0) {
          hero = new Hero(userInput);
          break;
        } else {
          System.out.println("Введите что нибудь");
        }
      } catch (NumberFormatException | IOException e) {
          System.out.println("Что-то пошло не так");
        }
    }
    System.out.println();


    while(num < 10){
      dvizhok.getDerevo().menu(hero.getName());

      hero.setHp(dvizhok.getDerevo().vstrechi.get(0).forAction(hero.getHp()));
      Node derevo = dvizhok.getDerevo(); //дерево
      Vstrecha vstrecha = derevo.vstrechi.get(0); // это встреча
      hero.setHp(vstrecha.procesActions(hero.getHp()));

      dvizhok.setDerevo(derevo.getFirstChild()); //получение первого ребенка



      if (hero.getHp() <= 0) {
        System.out.println();
        System.out.println("Игра окончена");
        break;
      } else {
          System.out.println("\n");
          System.out.println("на данный момент " + hero.getName() + " у тебя: " + hero.getHp());
          System.out.println("\n");
      }

      num++;
    }

    if(num == 10) {
      System.out.println("Победа. Вы прошли игру");
    }
  }
}
