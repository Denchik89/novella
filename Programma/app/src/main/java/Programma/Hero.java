package Programma;
import Programma.Vstrecha.*;
import Programma.Vstrecha.Drug.*;
import Programma.Vstrecha.Nicto.*;
import Programma.Vstrecha.Vrag.*;

public class Hero {
  private double hp = 100;
  private String name;

  Hero(String name) {
    this.name = name;
  }
  public double getHp() {
    return hp;
  }
  public void setHp(double hp) {
    this.hp = hp;
  }

  public String getName() {
    return name;
  }
}
