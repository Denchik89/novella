package Programma;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
  private Hero hero;

  @BeforeEach
  public void initilization() {
    hero = new Hero("Denis");

  }

  @Test void chekGetName() {
    assertEquals("Denis", hero.getName(), "good");
  }

  @Test void chekGetHp() {
    hero.setHp(100);
    assertEquals(100, hero.getHp(), "good");
  }

}
